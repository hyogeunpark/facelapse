package com.gpark.facelapse

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

class Application : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var sContext: Context
        fun getContext(): Context {
            return sContext
        }
    }
    override fun onCreate() {
        super.onCreate()
        sContext = this
    }
}