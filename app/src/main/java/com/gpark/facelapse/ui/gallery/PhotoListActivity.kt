package com.gpark.facelapse.ui.gallery

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.gpark.facelapse.R
import com.gpark.facelapse.common.BaseActivity
import com.gpark.facelapse.model.Gallery
import com.gpark.facelapse.utils.ImageUtils
import com.gpark.facelapse.utils.ViewSupportUtils
import kotlinx.android.synthetic.main.activity_photo_list.*
import java.io.File
import java.util.*

/**
 * TODO
 * 1. 공유
 */
class PhotoListActivity : BaseActivity(), View.OnClickListener {
    companion object {
        const val BROADCAST_DELETE_PHOTO = "broadcast delete photo"

        fun createInstance(context: Context?) {
            val intent = Intent(context, PhotoListActivity::class.java)
            context?.startActivity(intent)
        }
    }

    private val mAdapter: GalleryAdapter = GalleryAdapter()

    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == BROADCAST_DELETE_PHOTO) {
                val imagePath = intent.getStringExtra(PhotoActivity.EXTRA_IMAGE_PATH)
                mAdapter.deletePhoto(imagePath)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_list)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowTitleEnabled(false)
        }

        with(gallery_recycler_view) {
            addItemDecoration(GridSpacingItemDecoration(3, 4, false))
            adapter = mAdapter
            adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onChanged() { // notifyDataSetChanged()
                    super.onChanged()
                    visibleEmptyView(gallery_recycler_view.adapter.itemCount)
                }

                override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                    super.onItemRangeRemoved(positionStart, itemCount)
                    visibleEmptyView(gallery_recycler_view.adapter.itemCount)
                }

                override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                    super.onItemRangeChanged(positionStart, itemCount)
                    visibleEmptyView(itemCount)
                }

                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    visibleEmptyView(itemCount)
                }
            })
        }

        with(mAdapter) {
            gallery = getGalleryImages()
            notifyDataSetChanged()
        }

        photo_list_delete.setOnClickListener(this)
        photo_list_play.setOnClickListener(this)
        photo_list_shared.setOnClickListener(this)

        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, IntentFilter(BROADCAST_DELETE_PHOTO))
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver)
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.gallery, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_select -> {
                ViewSupportUtils.viewTransition(photo_list_bottom_navigation, mAdapter.isSelectMode())
                mAdapter.changeSelectMode()
                item.title = if (mAdapter.isSelectMode()) {
                    getString(R.string.eng_cancel)
                } else {
                    getString(R.string.eng_select)
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getGalleryImages(): ArrayList<Gallery> {

        val getImages = { filePath: String ->
            ArrayList(File(filePath).listFiles { _, name ->
                name.contains(ImageUtils.IMAGE_EXTENSION)
            }.asList())
        }

        val originImages = getImages(ImageUtils.DEFAULT_PICTURES_PATH)
        val thumbnailImages = getImages(ImageUtils.DEFAULT_THUMBNAILS_PATH)

        return ArrayList(originImages.zip(thumbnailImages)
            .map { (origin, thumbnail) ->
                Gallery(
                    absolutePath = origin.absolutePath,
                    thumbNailAbsolutePath = thumbnail.absolutePath,
                    fileName = origin.nameWithoutExtension
                )
            }.sortedBy {
                it.fileName.toLong()
            })
    }

    private fun visibleEmptyView(itemCount: Int) {
        val emptyViewVisibility = if (itemCount == 0) {
            View.VISIBLE
        } else {
            View.GONE
        }
        gallery_empty_view.visibility = emptyViewVisibility
        gallery_recycler_view.visibility = View.GONE - emptyViewVisibility
    }

    class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
            val position: Int = parent?.getChildAdapterPosition(view) ?: -1 // item position

            if (position >= 0) {
                val column: Int = position % spanCount // item column

                if (includeEdge) {
                    outRect?.left = spacing - column * spacing /
                            spanCount // spacing - column * ((1f / spanCount) * spacing)
                    outRect?.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect?.top = spacing
                    }
                    outRect?.bottom = spacing // item bottom
                } else {
                    outRect?.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                    outRect?.right = spacing - (column + 1) * spacing /
                            spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                    if (position >= spanCount) {
                        outRect?.top = spacing // item top
                    }
                }
            } else {
                outRect?.left = 0
                outRect?.right = 0
                outRect?.top = 0
                outRect?.bottom = 0
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            photo_list_delete -> mAdapter.deletePhotos()
            photo_list_play -> {
                val filterList = if (mAdapter.isSelectMode()) {
                    mAdapter.getSelectedPhotoList()
                        .takeIf {
                            it.isNotEmpty()
                        } ?: mAdapter.gallery
                } else {
                    mAdapter.gallery
                }
                PhotoActivity.Companion.Params(filterList.toTypedArray())
                    .play(true)
                    .build(v?.context)
            }
            photo_list_shared -> {
                photo_list_progress_bar.visibility = View.VISIBLE
                photo_list_progress_bar.progress = 0
                photo_list_progress_text.visibility = View.VISIBLE
                photo_list_progress_text.text = "진행 상황 : 0%"
                val filterList = if (mAdapter.isSelectMode()) {
                    mAdapter.getSelectedPhotoList()
                        .takeIf {
                            it.isNotEmpty()
                        } ?: mAdapter.gallery
                } else {
                    mAdapter.gallery
                }
                ImageUtils.createVideoFile("동영상임.mp4", filterList.map { gallery ->
                    Bitmap.createScaledBitmap(gallery.photo, gallery.photo.width / 2, gallery.photo.height / 2, true)
                }, { progress ->
                    runOnUiThread {
                        photo_list_progress_bar.progress = progress
                        photo_list_progress_text.text = "진행 상황 : $progress%"
                    }
                }) { isSuccess ->
                    Snackbar.make(
                        photo_list_view,
                        if (isSuccess) {
                            "Video 변환 성공"
                        } else {
                            "Video 변환 실패"
                        }, Snackbar.LENGTH_SHORT
                    ).show()
                    runOnUiThread {
                        photo_list_progress_bar.visibility = View.GONE
                        photo_list_progress_text.visibility = View.GONE
                    }
                }
//                Intent(Intent.ACTION_SEND).apply {
//                    putExtra(Intent.EXTRA_STREAM, Uri.parse())
//                    type = "image/*"
//                }.run {
//                    // Launch sharing dialog for image
//                    startActivity(Intent.createChooser(this@run, "Share Image"))
//                }
            }
        }
    }

    class GalleryAdapter : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {
        var gallery: ArrayList<Gallery> = ArrayList()
        private var selectMode: Boolean = false

        fun deletePhoto(imagePath: String) {
            gallery.indexOfFirst {
                it.absolutePath == imagePath
            }.takeIf {
                it != -1
            }?.run {
                gallery[this].getPhotoFile().delete()
                gallery[this].getThumbnailFile().delete()
                gallery.removeAt(this)
                notifyItemRemoved(this)
            }
        }

        fun deletePhotos() {
            gallery.filter {
                it.isSelected
            }.forEach { deletePhoto ->
                if (deletePhoto.getPhotoFile().delete() && deletePhoto.getThumbnailFile().delete()) {
                    gallery.remove(deletePhoto)
                }
            }
            notifyDataSetChanged()
        }

        fun getSelectedPhotoList(): List<Gallery> {
            return gallery
                .filter {
                    it.isSelected
                }
        }

        fun changeSelectMode() {
            selectMode = !selectMode
            notifyDataSetChanged()
        }

        fun isSelectMode(): Boolean {
            return selectMode
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_photo_list_item, parent, false))
        }

        override fun getItemCount(): Int {
            return gallery.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(gallery[position], selectMode)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private val mPhoto: ImageView by lazy { itemView.findViewById<ImageView>(R.id.photo_list_item_photo) }
            private val mDate: TextView by lazy { itemView.findViewById<TextView>(R.id.photo_list_item_date) }
            private val mCheck: CheckBox by lazy { itemView.findViewById<CheckBox>(R.id.photo_list_item_check) }

            init {
                itemView.setOnClickListener {
                    if (!isSelectMode()) {
                        PhotoActivity.Companion.Params(arrayOf(gallery[adapterPosition]))
                            .transitionView(mPhoto)
                            .build(it.context)
                    } else {
                        val filterList = getSelectedPhotoList()
                        val position = filterList.indexOf(gallery[adapterPosition])
                        PhotoActivity.Companion.Params(filterList.toTypedArray())
                            .showingPosition(position)
                            .transitionView(mPhoto)
                            .build(it.context)
                    }
                }
                mCheck.setOnCheckedChangeListener { _, isChecked ->
                    gallery[adapterPosition].isSelected = isChecked
                }
            }

            fun bind(gallery: Gallery, selectMode: Boolean) {
                mPhoto.setImageBitmap(gallery.thumbnail)
                mDate.text = gallery.date
                mCheck.visibility = if (selectMode) View.VISIBLE else View.GONE
                mCheck.isChecked = gallery.isSelected
            }
        }
    }
}