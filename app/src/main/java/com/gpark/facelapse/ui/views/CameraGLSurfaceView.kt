package com.gpark.facelapse.ui.views

import android.content.Context
import android.graphics.PixelFormat
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceHolder
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

open class CameraGLSurfaceView : GLSurfaceView, GLSurfaceView.Renderer {

    private var mMaxTextureSize = 0

    private var mViewWidth: Int = 0
    private var mViewHeight: Int = 0

    protected var mRecordWidth = 720
    protected var mRecordHeight = 1280

    protected var mPictureWidth = 720
    protected var mPictureHeight = 1280

    protected val mFaceLapseCamera: FaceLapseCamera by lazy { FaceLapseCamera() }

    data class Viewport(
        var x: Int = 0,
        var y: Int = 0,
        var width: Int = 0,
        var height: Int = 0
    )

    protected var mDrawViewport = Viewport()

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        setEGLContextClientVersion(2)
        setEGLConfigChooser(8, 8, 8, 8, 0, 0)
        holder.setFormat(PixelFormat.RGBA_8888)
        setRenderer(this)
        renderMode = GLSurfaceView.RENDERMODE_WHEN_DIRTY
    }

    fun presetRecordingSize(width: Int, height: Int) {
        mRecordWidth = width
        mRecordHeight = height
        mFaceLapseCamera.setPreferPreviewSize(height, width)
    }

    fun presetPictureSize(width: Int, height: Int) {
        mPictureWidth = width
        mPictureHeight = height
    }

    open fun onSwitchCamera() {

    }

    open fun switchCamera() {
        queueEvent {
            mFaceLapseCamera.closeCamera()
            onSwitchCamera()
            mFaceLapseCamera.switchCamera()
            mFaceLapseCamera.tryOpenCamera { isSuccess ->
                if (isSuccess) {
                    resumePreview()
                }
            }
            requestRender()
        }
    }

    open fun resumePreview() {

    }

    override fun onPause() {
        mFaceLapseCamera.closeCamera()
        super.onPause()
    }

    /**
     * surface view render
     */
    override fun onDrawFrame(gl: GL10?) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        super.surfaceDestroyed(holder)
        mFaceLapseCamera.closeCamera()
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES20.glDisable(GLES20.GL_DEPTH_TEST)
        GLES20.glDisable(GLES20.GL_STENCIL_TEST)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)

        val textureSize = IntArray(1)

        GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE, textureSize, 0)
        mMaxTextureSize = textureSize[0]
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        GLES20.glClearColor(0f, 0f, 0f, 0f)

        mViewWidth = width
        mViewHeight = height

        calcViewport()
    }

    private fun calcViewport() {
        val scaling = mRecordWidth / mRecordHeight.toFloat()
        val viewRatio = mViewWidth / mViewHeight.toFloat()
        val s = scaling / viewRatio

        if (s > 1.0) {
            mDrawViewport.width = (mViewHeight * scaling).toInt()
            mDrawViewport.height = mViewHeight
        } else {
            mDrawViewport.width = mViewWidth
            mDrawViewport.height = (mViewWidth / scaling).toInt()
        }

        mDrawViewport.x = (mViewWidth - mDrawViewport.width) / 2
        mDrawViewport.y = (mViewHeight - mDrawViewport.height) / 2
    }
}