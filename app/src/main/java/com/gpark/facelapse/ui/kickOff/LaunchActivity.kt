package com.gpark.facelapse.ui.kickOff

import android.app.Activity
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.gpark.facelapse.R
import com.gpark.facelapse.ui.MainActivity
import kotlinx.android.synthetic.main.activity_launch.*

class LaunchActivity : Activity() {

    companion object {
        private var sInitialized: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }

    override fun onResume() {
        super.onResume()
        if (sInitialized) {
            route()
        } else {
            launch_image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in_translate_up))
            launch_root.postDelayed({
                sInitialized = true
                route()
            }, 3000)
        }
    }

    private fun route() {
        MainActivity.createInstance(this)
        finish()
    }
}