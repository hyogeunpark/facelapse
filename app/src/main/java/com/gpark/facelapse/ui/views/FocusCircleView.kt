package com.gpark.facelapse.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

class FocusCircleView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var haveTouch = false
    private var touchArea: Rect? = null
    private val mPaint: Paint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.STROKE
        strokeWidth = 10f
    }

    fun setHaveTouch(haveTouch: Boolean, rect: Rect) {
        this.haveTouch = haveTouch
        this.touchArea = rect
        pivotX = rect.exactCenterX()
        pivotY = rect.exactCenterY()
    }

    override fun onDraw(canvas: Canvas) {
        if (haveTouch) {
            canvas.drawCircle(
                touchArea?.centerX()?.toFloat() ?: 0f,
                touchArea?.centerY()?.toFloat() ?: 0f,
                150f,
                mPaint
            )
        }
    }
}