package com.gpark.facelapse.ui.camera

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.hardware.Camera
import android.media.MediaActionSound
import android.os.Bundle
import android.os.CountDownTimer
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import com.gpark.facelapse.R
import com.gpark.facelapse.ui.gallery.PhotoListActivity
import com.gpark.facelapse.ui.views.FaceLapseDialog
import com.gpark.facelapse.utils.DateUtils
import com.gpark.facelapse.utils.ImageUtils
import com.gpark.facelapse.utils.ViewSupportUtils
import kotlinx.android.synthetic.main.fragment_camera.*
import java.util.*


/**
 * TODO
 * 1. setting view 만들기
 * 2. push notification
 */
class CameraFragment : Fragment(), View.OnClickListener {

    private val REQUEST_CAMERA_PERMISSION: Int = 1

    private lateinit var mRootView: View
    private val mShutterSound by lazy { MediaActionSound() }
    private var mTimer: Timer = Timer.TIME_0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mRootView = inflater.inflate(R.layout.fragment_camera, container, false)
        return mRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        initCamera()
    }

    private fun initViews() {
        // current date
        main_lower_date.text = DateUtils.convertDateToString(DateUtils.getCurrentTime())

        context?.let { context ->
            val states = arrayOf(
                intArrayOf(android.R.attr.state_selected), // selected
                intArrayOf(-android.R.attr.state_selected) // unselected
            )

            val colors = intArrayOf(
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorAccent)
            )

            main_lower_timer.imageTintList = ColorStateList(states, colors)
        }
        // click listener
        main_lower_rotation.setOnClickListener(this)
        main_lower_shutter.setOnClickListener(this)
        main_lower_gallery.setOnClickListener(this)
        main_lower_filter.setOnClickListener(this)
        main_lower_filter_change_value.setOnClickListener(this)
        main_lower_timer.setOnClickListener(this)

        // checked listener
        main_upper_screen.setOnCheckedChangeListener { _, isChecked ->
            ViewSupportUtils.viewTransition(main_lower_filter_bg, isChecked)
            val width = main_camera.measuredWidth
            val height = if (isChecked) {
                main_camera.measuredWidth + main_lower_filter_bg.measuredHeight
            } else {
                main_camera.measuredWidth
            }
            main_camera.presetPictureSize(width, height)
        }
    }

    fun calculateView() {
        val screenWidth = ViewSupportUtils.getScreenWidth(context)
        val screenHeight = ViewSupportUtils.getScreenHeight(context)

        // lower view size
        val lowerHeight = screenHeight - screenWidth - main_lower_filter_bg.height

        main_lower_bg.layoutParams.takeIf {
            it.height != lowerHeight
        }?.apply {
            height = lowerHeight
        }?.run {
            main_lower_bg.layoutParams = this
        }
    }

    private fun initCamera() {
        val cameraWidth = ViewSupportUtils.getScreenWidth(context)
        val cameraHeight = ViewSupportUtils.getScreenHeight(context)
        with(main_camera) {
            presetRecordingSize(cameraWidth, cameraHeight)
            presetPictureSize(cameraWidth, cameraWidth)

            setCameraSize(cameraWidth, cameraHeight, true) // > 4MP
            setZOrderOnTop(false)
            setZOrderMediaOverlay(true)
            setFocusView(main_camera_focus_view)
        }
    }

    private fun checkCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            Objects.requireNonNull<Activity>(activity),
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED
    }

    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            FaceLapseDialog(context)
                .title(getString(R.string.title_permission))
                .contents(getString(R.string.info_permission))
                .positive(getString(R.string.confirm), View.OnClickListener {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
                })
                .negative(getString(R.string.cancel), View.OnClickListener {
                    showErrorMessage()
                })
                .show()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.size != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showErrorMessage()
                return
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showErrorMessage() {
        FaceLapseDialog(context)
            .title(getString(R.string.title_permission))
            .contents(getString(R.string.error_permission))
            .positive(getString(R.string.confirm), View.OnClickListener {
                activity?.finish()
            })
            .cancelable(false)
            .show()
    }

    override fun onResume() {
        super.onResume()
        if (checkCameraPermission()) {
            requestCameraPermission()
        } else {
            main_camera.onResume()
            if (main_camera.isSmoothing) {
                main_camera.post {
                    main_camera.beautyFaceFilter()
                }
            }
        }
    }

    override fun onPause() {
        with(main_camera) {
            release()
            onPause()
        }
        super.onPause()
    }

    fun takePicture() {
        main_lower_shutter.performClick()
    }

    override fun onClick(v: View?) {
        when (v) {
            main_lower_filter_change_value -> {
                val dialogView = LayoutInflater.from(v?.context).inflate(R.layout.dialog_input, null)

                dialogView.findViewById<SeekBar>(R.id.dialog_seek_bar)
                    .setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                        override fun onStartTrackingTouch(seekBar: SeekBar?) {
                        }

                        override fun onStopTrackingTouch(seekBar: SeekBar?) {
                        }

                        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                            val intensity = progress / 100.0f
                            dialogView.findViewById<TextView>(R.id.dialog_seek_bar_value).text =
                                    "Intensity = $intensity"
                        }

                    })

                v?.context?.let {
                    AlertDialog.Builder(it)
                        .apply {
                            setTitle("필터 값 조정") // 제목 설정
                            setMessage("") // 내용 설정
                            setView(dialogView)
                            setPositiveButton("확인") { dialog, _ ->
                                val intensity = dialogView.findViewById<SeekBar>(R.id.dialog_seek_bar).progress / 100.0f
                                main_camera.setFilterIntensity(intensity)
                                dialog.dismiss()
                            }
                        }.show()
                }
            }
            main_lower_rotation -> {
                main_camera.switchCamera()
            }
            main_lower_filter -> {
                main_camera.changeFilter()
                Snackbar.make(
                    mRootView, if (main_camera.isSmoothing) {
                        "Beauty Effect On"
                    } else {
                        "Beauty Effect Off"
                    }, Snackbar.LENGTH_SHORT
                ).show()
            }
            main_lower_shutter -> {
                object : CountDownTimer(mTimer.seconds * 1000L, 1000L) {
                    override fun onFinish() {
                        main_camera.takePicture(callback = { image ->
                            mShutterSound.play(MediaActionSound.SHUTTER_CLICK)
                            ImageUtils.createImageFile(
                                "${System.currentTimeMillis()}${ImageUtils.IMAGE_EXTENSION}",
                                image
                            ) { isSuccess ->
                                Snackbar.make(
                                    mRootView, if (isSuccess) {
                                        "이미지 저장 완료"
                                    } else {
                                        "이미지 저장 실패"
                                    }, Snackbar.LENGTH_SHORT
                                ).show()
                                image.recycle()
                            }
                        })
                        main_camera_timer.text = (mTimer.seconds - 1).toString()
                        main_camera_timer.visibility = View.GONE
                    }

                    override fun onTick(millisUntilFinished: Long) {
                        main_camera_timer.text = (millisUntilFinished / 1000).toInt().toString()
                    }

                }.apply {
                    main_camera_timer.visibility = View.VISIBLE
                    start()
                }
            }
            main_lower_gallery -> PhotoListActivity.createInstance(context)
            main_lower_timer -> {
                mTimer = Timer.valueOf((mTimer.ordinal + 1) % Timer.values().count())
                main_camera_timer.text = mTimer.seconds.toString()
                main_lower_timer_seconds.text = String.format(Locale.KOREA, "%d초", (mTimer.seconds - 1))
                if (mTimer != Timer.TIME_0) {
                    main_lower_timer.isSelected = true
                    main_lower_timer_seconds.visibility = View.VISIBLE
                } else {
                    main_lower_timer.isSelected = false
                    main_lower_timer_seconds.visibility = View.GONE
                }


            }
        }
    }

    enum class Timer(val seconds: Int) {
        TIME_0(0), TIME_3(4), TIME_5(6), TIME_10(11);

        companion object {
            fun valueOf(ordinal: Int): Timer = when (ordinal) {
                1 -> TIME_3
                2 -> TIME_5
                3 -> TIME_10
                else -> TIME_0
            }
        }
    }
}