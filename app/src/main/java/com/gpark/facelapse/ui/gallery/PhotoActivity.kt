package com.gpark.facelapse.ui.gallery

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.gpark.facelapse.R
import com.gpark.facelapse.common.BaseActivity
import com.gpark.facelapse.model.Gallery
import kotlinx.android.synthetic.main.activity_photo.*

/**
 * TODO
 * 1. 공유
 */
class PhotoActivity : BaseActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_IMAGE_PATH: String = "extra_image_path"
        const val EXTRA_IMAGES: String = "extra_images"
        const val EXTRA_SHOWING_POSITION: String = "extra_showing_position"
        const val EXTRA_PLAY: String = "extra_play"

        // TODO: builder pattern?
        class Params(val selectedList: Array<Gallery>) {
            var showingPosition = 0
            var transitionView: View? = null
            var isPlay = false

            fun showingPosition(position: Int): Params {
                showingPosition = position
                return this
            }

            fun transitionView(view: View?) : Params {
                transitionView = view
                return this
            }

            fun play(isPlay: Boolean) : Params {
                this.isPlay = isPlay
                return this
            }

            fun build(context: Context?) {
                if(selectedList.isEmpty() || showingPosition == -1) return
                createInstance(context, this)
            }
        }

        private fun createInstance(context: Context?, params: Params) {
            val intent = Intent(context, PhotoActivity::class.java)
                .apply {
                    putExtra(EXTRA_IMAGES, params.selectedList)
                    putExtra(EXTRA_SHOWING_POSITION, params.showingPosition)
                    putExtra(EXTRA_PLAY, params.isPlay)
                }

            params.transitionView?.let { view ->
                context.takeIf {
                    context is Activity
                }?.run {
                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        context as Activity,
                        view,
                        ViewCompat.getTransitionName(view)
                    )
                    context.startActivity(intent, options.toBundle())
                    return
                }
            }

            context?.startActivity(intent)
        }
    }

    private val mImages: ArrayList<Gallery> = ArrayList()
    private var mPosition = 0
    private val mAdapter by lazy { PhotoListAdapter(mImages) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        intent.getParcelableArrayExtra(EXTRA_IMAGES).filterIsInstance<Gallery>()
            .forEach {
                mImages.add(it)
            }
        mPosition = intent.getIntExtra(EXTRA_SHOWING_POSITION, 0)
        initView()
    }

    private fun initView() {
        changeImage(mImages[mPosition].photo)
        if (mImages.count() == 1) {
            photo_list.visibility = View.GONE
        } else {
            photo_list.visibility = View.VISIBLE
            with(photo_list) {
                val layoutManger = LinearLayoutManager(context)
                    .apply {
                        orientation = LinearLayoutManager.HORIZONTAL
                    }
                layoutManager = layoutManger
                adapter = mAdapter
            }
        }

        mAdapter.notifyDataSetChanged()

        photo_play.setOnClickListener(this)
        photo_delete.setOnClickListener(this)
        photo_shared.setOnClickListener(this)

        val isPlay = intent.getBooleanExtra(EXTRA_PLAY, false)
        if(isPlay) {
            // TODO : 다른 방법 찾아보기
            photo_play.postDelayed({
                photo_play.performClick()
            }, 500)
        }
    }

    private fun changeImage(bitmap: Bitmap) {
        photo_frame.setImageBitmap(bitmap)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v) {
            photo_delete -> {
                val currentFile = mImages[mPosition].getPhotoFile()
                if (currentFile.delete()) {
                    val intent = Intent(PhotoListActivity.BROADCAST_DELETE_PHOTO)
                        .apply {
                            putExtra(EXTRA_IMAGE_PATH, mImages[mPosition].absolutePath)
                        }
                    LocalBroadcastManager.getInstance(photo_delete.context).sendBroadcast(intent)
                    mImages.removeAt(mPosition)
                    mAdapter.notifyItemRemoved(mPosition)
                    if (mImages.count() == 0) {
                        finish()
                    } else {
                        changeImage(mImages[--mPosition].photo)
                    }
                } else {
                    Snackbar.make(photo_delete.rootView, getString(R.string.error_file_delete), Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
            photo_play -> {
                val animationDrawable = AnimationDrawable()
                animationDrawable.isOneShot = true
                mImages.map {
                    BitmapDrawable(it.photo)
                }.forEach {
                    animationDrawable.addFrame(it, 100)
                }
                photo_frame.setImageDrawable(animationDrawable)
                animationDrawable.start()
                mPosition = mImages.count() - 1
            }
            photo_shared -> {
                Intent(Intent.ACTION_SEND).apply {
                    putExtra(Intent.EXTRA_STREAM, Uri.parse(mImages[mPosition].absolutePath))
                    type = "image/*"
                }.run {
                    // Launch sharing dialog for image
                    startActivity(Intent.createChooser(this@run, "Share Image"))
                }
            }
        }
    }

    inner class PhotoListAdapter(val gallery: ArrayList<Gallery>) :
        RecyclerView.Adapter<PhotoListAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(ImageView(parent.context).apply {
                layoutParams = ViewGroup.LayoutParams(parent.measuredHeight, parent.measuredHeight)
                scaleType = ImageView.ScaleType.CENTER_CROP
            })
        }

        override fun getItemCount(): Int = gallery.count()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(gallery[position])
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            init {
                itemView.setOnClickListener {
                    mPosition = adapterPosition
                    changeImage(gallery[adapterPosition].photo)
                }
            }

            fun bind(photo: Gallery) {
                (itemView as ImageView).setImageBitmap(photo.photo)
            }
        }
    }
}