package com.gpark.facelapse.ui.views

import android.R.attr.duration
import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.opengl.GLES11Ext
import android.opengl.GLES20
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationSet
import org.wysaid.common.FrameBufferObject
import org.wysaid.nativePort.CGEFrameRecorder
import java.nio.IntBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class CameraGLSurfaceTextureView : CameraGLSurfaceView, SurfaceTexture.OnFrameAvailableListener {

    private val BEAUTY_FACE: String = "@beautify face 1 720 1280"
    private val BEAUTY_NONE = ""
    private var mSurfaceTexture: SurfaceTexture? = null
    private var mTextureID: Int = 0
    private var mIsTransformMatrixSet = false
    private var mFrameRecorder: CGEFrameRecorder? = null
    private var mTransformMatrix = FloatArray(16)
    private var mMeteringAreaSupported = false
    private var mFocusCircleView: FocusCircleView? = null
    var isSmoothing: Boolean = false

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    @Synchronized
    fun setFilterWithConfig(config: String) {
        queueEvent {
            mFrameRecorder?.setFilterWidthConfig(config)
        }
    }

    @Synchronized
    fun setFilterIntensity(intensity: Float) {
        queueEvent {
            mFrameRecorder?.setFilterIntensity(intensity)
        }
    }

    fun release() {
        mSurfaceTexture?.release()
        mSurfaceTexture = null

        if (mTextureID != 0) {
            deleteTextureID(mTextureID)
            mTextureID = 0
        }

        mFrameRecorder?.release()
        mFrameRecorder = null
    }

    override fun resumePreview() {
        if (!mFaceLapseCamera.isCameraOpened) {
            mFaceLapseCamera.openCamera()
        }

        if (!mFaceLapseCamera.isPreviewing) {
            mFaceLapseCamera.startPreview(mSurfaceTexture)
            mFrameRecorder?.srcResize(mFaceLapseCamera.previewHeight(), mFaceLapseCamera.previewWidth())
        }

        requestRender()
    }

    fun setCameraSize(width: Int, height: Int, isBigger: Boolean) {
        mFaceLapseCamera.setPictureSize(height, width, isBigger)
    }

    override fun onSwitchCamera() {
        super.onSwitchCamera()
        mFrameRecorder?.setSrcRotation((Math.PI / 2.0).toFloat())
        mFrameRecorder?.setRenderFlipScale(1.0f, -1.0f)
    }

    override fun onFrameAvailable(surfaceTexture: SurfaceTexture?) {
        requestRender()
    }

    override fun onDrawFrame(gl: GL10?) {
        super.onDrawFrame(gl)
        if (mSurfaceTexture == null || !isShown) {
            return
        }

        mSurfaceTexture?.updateTexImage()

        mSurfaceTexture?.getTransformMatrix(mTransformMatrix)
        mFrameRecorder?.update(mTextureID, mTransformMatrix)

        mFrameRecorder?.runProc()

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0)
        GLES20.glClearColor(0f, 0f, 0f, 0f)
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)

        mFrameRecorder?.render(mDrawViewport.x, mDrawViewport.y, mDrawViewport.width, mDrawViewport.height)
    }

    @SuppressLint("Recycle")
    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        mFrameRecorder = CGEFrameRecorder()
        mIsTransformMatrixSet = false
        if (mFrameRecorder?.init(mRecordWidth, mRecordHeight, mRecordWidth, mRecordHeight) == false) {
            Log.e(CameraGLSurfaceTextureView::class.simpleName, "Frame Recorder init failed!")
        }

        mFrameRecorder?.setSrcRotation((Math.PI / 2.0).toFloat())
        mFrameRecorder?.setSrcFlipScale(1.0f, -1.0f)
        mFrameRecorder?.setRenderFlipScale(1.0f, -1.0f)

        mTextureID = getSurfaceTextureID()
        mSurfaceTexture = SurfaceTexture(mTextureID)
        mSurfaceTexture?.setOnFrameAvailableListener(this)

        super.onSurfaceCreated(gl, config)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        super.onSurfaceChanged(gl, width, height)

        if (!mFaceLapseCamera.isPreviewing) {
            resumePreview()
        }

        mMeteringAreaSupported = mFaceLapseCamera.isAreaSupported()
    }

    fun takePicture(callback: ((Bitmap) -> (Unit))) {

        queueEvent {
            val frameBufferObject = FrameBufferObject()
            val bufferTexID: Int = genBlankTextureID(mRecordWidth, mRecordHeight)
            val buffer: IntBuffer = IntBuffer.allocate(mRecordWidth * mRecordHeight)
            val bmp: Bitmap = Bitmap.createBitmap(mRecordWidth, mRecordHeight, Bitmap.Config.ARGB_8888)

            frameBufferObject.bindTexture(bufferTexID)
            GLES20.glViewport(0, 0, mRecordWidth, mRecordHeight)
            mFrameRecorder?.drawCache()
            GLES20.glReadPixels(0, 0, mRecordWidth, mRecordHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer)
            bmp.copyPixelsFromBuffer(buffer)

            frameBufferObject.release()
            GLES20.glDeleteTextures(1, intArrayOf(bufferTexID), 0)

            val result: Bitmap = Bitmap.createBitmap(bmp, 0, 0, mPictureWidth, mPictureHeight)

            bmp.recycle()
            callback(result)
        }
    }

    // @beautify bilateral 100 3.5 2
    // @beautify face 1 480 640
    fun changeFilter() {
        isSmoothing = !isSmoothing
        if (isSmoothing) {
            beautyFaceFilter()
        } else {
            noneFilter()
        }
    }

    fun beautyFaceFilter() {
        setFilterWithConfig(BEAUTY_FACE)
    }

    private fun noneFilter() {
        setFilterWithConfig(BEAUTY_NONE)
    }

    fun setFocusView(view: View) {
        if (view is FocusCircleView) {
            mFocusCircleView = view
        }
    }

    private fun focusRectangle(x: Float, y: Float, coefficient: Float) {
        val focusArea = calculateTapArea(x, y, coefficient)
        if (mMeteringAreaSupported) {
            mFaceLapseCamera.setMeteringAreas(focusArea, 1000)
        }
    }

    private fun calculateTapArea(x: Float, y: Float, coefficient: Float): Rect {
        val areaSize = (/*focusAreaSize*/100 * coefficient).toInt()
        val clamp = { value: Int, min: Int, max: Int ->
            Int
            when {
                value > max -> max
                value < min -> min
                else -> value
            }
        }
        val left = clamp(x.toInt() - areaSize / 2, 0, super.getWidth() - areaSize)
        val top = clamp(y.toInt() - areaSize / 2, 0, super.getHeight() - areaSize)

        return RectF(left.toFloat(), top.toFloat(), (left + areaSize).toFloat(), (top + areaSize).toFloat())
            .apply {
                matrix.mapRect(this)
            }.run {
                Rect(Math.round(this.left), Math.round(this.top), Math.round(this.right), Math.round(this.bottom))
            }
    }

    private fun focusAtPoint(x: Float, y: Float, callback: Camera.AutoFocusCallback) {
        mFaceLapseCamera.focusAtPoint(x, y, 0.2f, callback)
    }

    private fun setFocusMode(focusMode: String) {
        mFaceLapseCamera.setFocusMode(focusMode)
    }


    private fun getSurfaceTextureID(): Int {
        val texID = IntArray(1)
        GLES20.glGenTextures(1, texID, 0)
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, texID[0])

        GLES20.glTexParameterf(
            GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
            GLES20.GL_TEXTURE_MIN_FILTER,
            GLES20.GL_LINEAR.toFloat()
        )
        GLES20.glTexParameterf(
            GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
            GLES20.GL_TEXTURE_MAG_FILTER,
            GLES20.GL_LINEAR.toFloat()
        )
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
        return texID[0]
    }

    private fun deleteTextureID(texID: Int) {
        GLES20.glDeleteTextures(1, intArrayOf(texID), 0)
    }

    private fun genBlankTextureID(
        width: Int,
        height: Int,
        filter: Int = GLES20.GL_LINEAR,
        wrap: Int = GLES20.GL_CLAMP_TO_EDGE
    ): Int {
        val texID = IntArray(1)
        GLES20.glGenTextures(1, texID, 0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texID[0])
        GLES20.glTexImage2D(
            GLES20.GL_TEXTURE_2D,
            0,
            GLES20.GL_RGBA,
            width,
            height,
            0,
            GLES20.GL_RGBA,
            GLES20.GL_UNSIGNED_BYTE,
            null
        )
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, filter.toFloat())
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, filter.toFloat())
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, wrap)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, wrap)
        return texID[0]
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let {
            if (it.actionMasked == MotionEvent.ACTION_DOWN) {
                val focusX = it.x / width
                val focusY = it.y / height

                focusRectangle(it.x, it.y, 1.5f)

                focusAtPoint(focusX, focusY, Camera.AutoFocusCallback { success, _ ->
                    if (!success) {
                        setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)
                    }
                })

                // focus시에 생기는 Circle View
                mFocusCircleView?.let { focusCircleView ->
                    val touchRect = Rect(
                        (it.x - 100).toInt(),
                        (it.y - 100).toInt(),
                        (it.x + 100).toInt(),
                        (it.y + 100).toInt()
                    )
                    focusCircleView.setHaveTouch(true, touchRect)
                    focusCircleView.invalidate()
                    val scaleOnX = ObjectAnimator.ofFloat(focusCircleView, "scaleX", 0.7f)
                        .apply {
                            duration = 300
                        }
                    val scaleOnY = ObjectAnimator.ofFloat(focusCircleView, "scaleY", 0.7f)
                        .apply {
                            duration = 300
                        }

                    AnimatorSet().apply {
                        addListener(object: Animator.AnimatorListener {
                            override fun onAnimationRepeat(animation: Animator?) {
                            }

                            override fun onAnimationEnd(animation: Animator?) {
                                focusCircleView.setHaveTouch(false, Rect(0, 0, 0, 0))
                                focusCircleView.scaleX = 1.0f
                                focusCircleView.scaleY = 1.0f
                                focusCircleView.invalidate()
                            }

                            override fun onAnimationCancel(animation: Animator?) {
                            }

                            override fun onAnimationStart(animation: Animator?) {
                            }

                        })
                    }.run {
                        this.play(scaleOnX).with(scaleOnY)
                        this.start()
                    }

                }
            }
        }

        return super.onTouchEvent(event)
    }
}