package com.gpark.facelapse.ui.views

import android.app.Dialog
import android.content.Context
import android.view.View
import com.gpark.facelapse.R
import kotlinx.android.synthetic.main.dialog_base.*

class FaceLapseDialog(context: Context?) : Dialog(context, R.style.AppTheme_Dialog) {

    init {
        super.setContentView(R.layout.dialog_base)
        dialog_negative.visibility = View.GONE
        dialog_title.visibility = View.GONE
        dialog_positive.setOnClickListener {
            dismiss()
        }
    }

    fun title(title: String): FaceLapseDialog {
        dialog_title.visibility = View.VISIBLE
        dialog_title.text = title
        return this
    }

    fun contents(contents: String): FaceLapseDialog {
        dialog_contents.text = contents
        return this
    }

    fun positive(
        positive: String,
        callback: View.OnClickListener? = null
    ): FaceLapseDialog {
        with(dialog_positive) {
            setOnClickListener { view ->
                dismiss()
                callback?.onClick(view)
            }
            text = positive
        }

        return this
    }

    fun negative(
        negative: String,
        callback: View.OnClickListener? = null
    ): FaceLapseDialog {
        with(dialog_negative) {
            visibility = View.GONE
            setOnClickListener { view ->
                dismiss()
                callback?.onClick(view)
            }
            text = negative
        }

        return this
    }

    fun cancelable(cancelable: Boolean): FaceLapseDialog {
        super.setCancelable(cancelable)
        return this
    }
}