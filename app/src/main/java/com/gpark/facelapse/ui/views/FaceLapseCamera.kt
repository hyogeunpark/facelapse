package com.gpark.facelapse.ui.views

import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.util.Log
import java.io.IOException
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class FaceLapseCamera {

    private val TAG = FaceLapseCamera::class.simpleName

    var isPreviewing: Boolean = false
    var isCameraOpened: Boolean = false
        get() = mCameraDevice != null

    private val mCameraOpenCloseLock = Semaphore(1)
    private var mFacing = Camera.CameraInfo.CAMERA_FACING_FRONT

    private var mCameraDevice: Camera? = null
    private lateinit var mParams: Camera.Parameters

    private var mPreviewWidth: Int = 0
    private var mPreviewHeight: Int = 0

    private var mPictureWidth = 1440
    private var mPictureHeight = 2560

    private var mPreferPreviewWidth = 480
    private var mPreferPreviewHeight = 640

    fun setPreferPreviewSize(w: Int, h: Int) {
        mPreferPreviewHeight = w
        mPreferPreviewWidth = h
    }

    fun previewHeight(): Int = mPreviewHeight

    fun previewWidth(): Int = mPreviewWidth

    @Synchronized
    fun openCamera() {
        tryOpenCamera { isSuccess ->
            if (!isSuccess) {
                Log.e(TAG, "Fail camera open!!")
            }
        }
    }

    fun startPreview(texture: SurfaceTexture?) {
        if (isPreviewing || mCameraDevice == null || texture == null) {
            return
        }
        try {
            mCameraDevice?.setPreviewTexture(texture)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        mCameraDevice?.startPreview()
        isPreviewing = true
    }

    @Synchronized
    fun stopPreview() {
        if (isPreviewing) {
            isPreviewing = false
            mCameraDevice?.stopPreview()
        }
    }

    @Synchronized
    fun tryOpenCamera(onSuccessCallback: (Boolean) -> Unit) {
        if (mCameraDevice != null || !mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
            mCameraOpenCloseLock.release()
            onSuccessCallback(true)
            return
        }

        var result = false

        try {
            stopPreview()
            mCameraDevice?.release()
            mCameraDevice = null
            mCameraDevice = Camera.open(mFacing)
        } catch (e: Exception) {
            e.printStackTrace()
            mCameraDevice = null
        }

        if (mCameraDevice != null) {
            try {
                initCamera()
            } catch (e: Exception) {
                mCameraDevice?.release()
                mCameraDevice = null
            }
            result = true
        }

        mCameraOpenCloseLock.release()
        onSuccessCallback(result)
    }

    private fun initCamera() {
        mCameraDevice?.let { cameraDevice ->
            mParams = cameraDevice.parameters
            mParams.pictureFormat = PixelFormat.JPEG

            val pictureSize: Camera.Size? = mParams.supportedPictureSizes
                .maxBy {
                    it.width >= mPictureWidth && it.height >= mPictureHeight
                } ?: return

            val previewSize: Camera.Size? = mParams.supportedPreviewSizes
                .maxBy {
                    it.width >= mPreferPreviewWidth && it.height >= mPreferPreviewHeight
                } ?: return

            val fpsMax = mParams.supportedPreviewFrameRates.max() ?: 30

            with(mParams) {
                setPreviewSize(previewSize!!.width, previewSize.height)
                setPictureSize(pictureSize!!.width, pictureSize.height)
                previewFrameRate = fpsMax
            }

            try {
                cameraDevice.parameters = mParams
            } catch (e: Exception) {
                e.printStackTrace()
            }

            mParams = cameraDevice.parameters

            val cameraPictureSize = mParams.pictureSize
            val cameraPreviewSize = mParams.previewSize

            mPreviewWidth = cameraPreviewSize.width
            mPreviewHeight = cameraPreviewSize.height

            mPictureWidth = cameraPictureSize.width
            mPictureHeight = cameraPictureSize.height

            Log.i(TAG, String.format("Camera Picture Size: %d x %d", cameraPictureSize.width, cameraPictureSize.height))
            Log.i(TAG, String.format("Camera Preview Size: %d x %d", cameraPreviewSize.width, cameraPreviewSize.height))
        } ?: return
    }

    @Synchronized
    fun setFocusMode(focusMode: String) {
        if (mCameraDevice == null)
            return

        mParams = mCameraDevice?.parameters!!
        val focusModes = mParams.supportedFocusModes
        if (focusModes.contains(focusMode)) {
            mParams.focusMode = focusMode
        }
    }

    @Synchronized
    fun setPictureSize(width: Int, height: Int, isBigger: Boolean) {
        if (mCameraDevice == null) {
            mPictureWidth = width
            mPictureHeight = height
            return
        }

        mParams = mCameraDevice?.parameters!!

        val pictureSize: Camera.Size? = if (isBigger) {
            mParams.supportedPictureSizes
                .maxBy {
                    it.width >= width && it.height >= height
                }
        } else {
            mParams.supportedPictureSizes
                .minBy {
                    it.width <= width && it.height <= height
                }
        }

        mPictureWidth = pictureSize?.width ?: 0
        mPictureHeight = pictureSize?.height ?: 0

        try {
            mParams.setPictureSize(mPictureWidth, mPictureHeight)
            mCameraDevice?.parameters = mParams
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun isAreaSupported(): Boolean = (mCameraDevice?.parameters?.maxNumMeteringAreas ?: 0) > 0

    fun setMeteringAreas(focusArea: Rect, weight: Int) {
        mCameraDevice?.parameters?.meteringAreas = arrayListOf(Camera.Area(focusArea, weight))
    }

    fun focusAtPoint(x: Float, y: Float, callback: Camera.AutoFocusCallback) {
        focusAtPoint(x, y, 0.2f, callback)
    }

    @Synchronized
    fun focusAtPoint(x: Float, y: Float, radius: Float, callback: Camera.AutoFocusCallback) {
        if (mCameraDevice == null) {
            Log.e(TAG, "Error: focus after release.")
            return
        }

        mParams = mCameraDevice?.parameters!!

        if (mParams.maxNumMeteringAreas > 0) {

            val focusRadius = (radius * 1000.0f).toInt()
            val left = (x * 2000.0f - 1000.0f).toInt() - focusRadius
            val top = (y * 2000.0f - 1000.0f).toInt() - focusRadius

            val focusArea = Rect()
                .apply {
                    this.left = Math.max(left, -1000)
                    this.top = Math.max(top, -1000)
                    this.right = Math.min(left + focusRadius, 1000)
                    this.bottom = Math.min(top + focusRadius, 1000)
                }

            val meteringAreas = ArrayList<Camera.Area>()
                .apply {
                    this.add(Camera.Area(focusArea, 800))
                }

            try {
                mCameraDevice?.cancelAutoFocus()
                mCameraDevice?.parameters = mParams.apply {
                    focusMode = Camera.Parameters.FOCUS_MODE_AUTO
                    focusAreas = meteringAreas
                }
                mCameraDevice?.autoFocus(callback)
            } catch (e: Exception) {
                Log.e(TAG, "Error: focusAtPoint failed: " + e.toString())
            }

        } else {
            try {
                mCameraDevice?.autoFocus(callback)
            } catch (e: Exception) {
                Log.e(TAG, "Error: focusAtPoint failed: " + e.toString())
            }

        }
    }

    fun closeCamera() {
        try {
            mCameraOpenCloseLock.acquire()
            stopPreview()
            mCameraDevice?.setPreviewCallback(null)
            mCameraDevice?.release()
            mCameraDevice = null
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            mCameraOpenCloseLock.release()
        }
    }

    fun switchCamera() {
        mFacing = if (mFacing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            Camera.CameraInfo.CAMERA_FACING_BACK
        } else {
            Camera.CameraInfo.CAMERA_FACING_FRONT
        }
    }
}