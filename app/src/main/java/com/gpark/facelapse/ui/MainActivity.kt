package com.gpark.facelapse.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.KeyEvent
import android.view.WindowManager
import com.gpark.facelapse.R
import com.gpark.facelapse.ui.camera.CameraFragment


class MainActivity : FragmentActivity() {

    private val mCameraFragment by lazy { CameraFragment() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.main_camera_view, mCameraFragment)
                .commit()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if(hasFocus) {
            mCameraFragment.calculateView()
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        return if(event?.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            mCameraFragment.takePicture()
            true
        } else {
            super.dispatchKeyEvent(event)
        }
    }
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
//    private external fun ConvertRGBtoGray(matAddrInput: Long?, matAddrResult: Long?)

    companion object {
        fun createInstance(context: Context?) {
            val intent = Intent(context, MainActivity::class.java)
            context?.startActivity(intent)
        }
    }
}
