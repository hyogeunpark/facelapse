package com.gpark.facelapse.model

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Parcel
import android.os.Parcelable
import com.gpark.facelapse.utils.DateUtils
import java.io.File
import java.util.*

data class Gallery(
    val absolutePath: String = "",
    val thumbNailAbsolutePath: String = "",
    val fileName: String = "",
    var isSelected: Boolean = false
) : Parcelable {

    val date by lazy {
        DateUtils.convertDateToString(Date(fileName.toLong()), "yy/MM/dd")
    }

    val thumbnail: Bitmap by lazy {
        BitmapFactory.decodeFile(thumbNailAbsolutePath)
    }

    val photo: Bitmap by lazy {
        BitmapFactory.decodeFile(absolutePath)
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    )

    fun getPhotoFile(): File = File(absolutePath)
    fun getThumbnailFile(): File = File(thumbNailAbsolutePath)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(absolutePath)
        parcel.writeString(thumbNailAbsolutePath)
        parcel.writeString(fileName)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Gallery?> {
        override fun createFromParcel(parcel: Parcel): Gallery? {
            return Gallery(parcel)
        }

        override fun newArray(size: Int): Array<Gallery?> {
            return arrayOfNulls(size)
        }
    }
}