package com.gpark.facelapse.utils

import android.content.Context
import android.view.View

object ViewSupportUtils {

    fun getScreenWidth(context: Context?): Int {
        return context?.applicationContext?.resources?.displayMetrics?.widthPixels ?: 0
    }

    fun getScreenWidthDp(context: Context?): Int {
        return (getScreenWidth(context) / (context?.applicationContext?.resources?.displayMetrics?.density
            ?: 1f)).toInt()
    }

    fun getScreenHeight(context: Context?): Int {
        return context?.applicationContext?.resources?.displayMetrics?.heightPixels ?: 0
    }

    fun getScreenHeightDp(context: Context?): Int {
        return (getScreenHeight(context) / (context?.applicationContext?.resources?.displayMetrics?.density
            ?: 1f)).toInt()
    }

    fun viewTransition(view: View, isVisible: Boolean) {
        if (isVisible) {
            view.animate().y((view.top + view.measuredHeight).toFloat())
        } else {
            view.animate().y(view.top.toFloat())
        }
    }
}