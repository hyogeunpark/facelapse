package com.gpark.facelapse.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    val DATEFORMAT_DEFAULT: String = "yyyy-MM-dd HH:mm:ss"

    fun getCurrentTime(): Date {
        return Date()
    }

    fun convertStringToDate(date: String) : Date {
        return convertStringToDate(date, DATEFORMAT_DEFAULT)
    }

    fun convertStringToDate(date: String, dateFormat: String) : Date {
        val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
        return simpleDateFormat.parse(date)
    }

    fun convertDateToString(date: Date) : String {
        return convertDateToString(date, DATEFORMAT_DEFAULT)
    }

    fun convertDateToString(date: Date, dateFormat: String) : String {
        val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
        return simpleDateFormat.format(date)
    }
}