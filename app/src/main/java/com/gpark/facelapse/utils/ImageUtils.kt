package com.gpark.facelapse.utils

import android.content.ContentValues
import android.graphics.Bitmap
import android.provider.MediaStore
import android.util.Log
import com.gpark.facelapse.Application
import org.jcodec.api.android.AndroidSequenceEncoder
import org.jcodec.common.io.FileChannelWrapper
import org.jcodec.common.io.NIOUtils
import org.jcodec.common.model.Rational
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


object ImageUtils {

    val DEFAULT_PICTURES_PATH: String = "${Application.getContext().getExternalFilesDir(null)}/pictures"
    val DEFAULT_THUMBNAILS_PATH: String = "${Application.getContext().getExternalFilesDir(null)}/pictures/thumbnails"
    private val DEFAULT_VIDEO_PATH: String = "${Application.getContext().getExternalFilesDir(null)}/videos"
    const val IMAGE_EXTENSION = ".jpg"

    fun createVideoFile(
        fileName: String,
        images: List<Bitmap>,
        progressRate: (Int) -> Unit,
        callback: (Boolean) -> Unit
    ) {
        checkExistsFile(DEFAULT_VIDEO_PATH)

        Thread(Runnable {
            var out: FileChannelWrapper? = null
            try {
                out = NIOUtils.writableFileChannel("$DEFAULT_VIDEO_PATH/$fileName")
                val encoder = AndroidSequenceEncoder(out, Rational.R(3, 1))

                images.forEachIndexed { index, bitmap ->
                    // Encode the image
                    encoder.encodeImage(bitmap)
                    val progress = (((index + 1) * 100.0f) / images.count()).toInt()
                    progressRate(progress)
                }
                encoder.finish()

                NIOUtils.closeQuietly(out)
                ContentValues(3)
                    .apply {
                        put(MediaStore.Video.Media.TITLE, "test")
                        put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
                        put(MediaStore.Video.Media.DATA, File("$DEFAULT_VIDEO_PATH/$fileName").absolutePath)
                    }.run {
                        Application.getContext().contentResolver.insert(
                            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            this
                        )
                    }
                callback(true)
            } catch (e: Exception) {
                e.printStackTrace()
                callback(false)
            } finally {
                out?.close()
            }
        }).apply {
            start()
        }
    }

    fun createImageFile(fileName: String, image: Bitmap, callback: (Boolean) -> Unit) {
        saveImage(DEFAULT_PICTURES_PATH, fileName, image, callback)
        createThumbnail(DEFAULT_THUMBNAILS_PATH, fileName, image)
    }

    private fun createThumbnail(thumbnailFilePath: String, fileName: String, image: Bitmap) {
        val thumbnailFileName = "thumb_$fileName"
        val thumbnailImage = Bitmap.createScaledBitmap(image, image.width / 4, image.height / 4, true)

        saveImage(thumbnailFilePath, thumbnailFileName, thumbnailImage, null)
    }

    private fun saveImage(filePath: String, fileName: String, image: Bitmap, callback: ((Boolean) -> Unit)?) {
        val imagePath = checkExistsFile(filePath)

        val file = File("$imagePath", fileName)
        Thread(ImageSaver(file, image) { isSuccess ->
            callback?.invoke(isSuccess)
        }).apply {
            start()
        }
    }

    private fun checkExistsFile(filePath: String): File {
        return File(filePath).apply {
            takeIf {
                !it.exists()
            }?.run {
                mkdirs()
            }
        }
    }

    class ImageSaver(private val file: File, private val bitmap: Bitmap, private val callback: (Boolean) -> Unit) :
        Runnable {
        override fun run() {
            var output: FileOutputStream? = null
            try {
                output = FileOutputStream(file)
                BufferedOutputStream(output)
                    .apply {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, this)
                        flush()
                        close()
                    }

            } catch (e: IOException) {
                e.printStackTrace()
                callback(false)
            } finally {
                output?.close()
            }
            callback(true)
        }
    }
}