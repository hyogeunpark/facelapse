#include <jni.h>
#include <string>
#include <android/log.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, "FaceLapse OpenCV", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, "FaceLapse OpenCV", __VA_ARGS__)

//extern "C" JNIEXPORT jstring JNICALL
//Java_com_gpark_facelapse_ui_MainActivity_stringFromJNI(
//        JNIEnv *env,
//        jobject /* this */) {
//    std::string hello = "Hello from C++";
//    return env->NewStringUTF(hello.c_str());
//}

extern "C" JNIEXPORT void JNICALL
Java_com_gpark_facelapse_utils_JNIUtils_convertRGBtoGray(
        JNIEnv *env,
        jobject instance,
        jlong matAddrInput,
        jlong matAddrResult) {


    Mat &matInput = *(Mat *) matAddrInput;
    Mat &matResult = *(Mat *) matAddrResult;

    cvtColor(matInput, matResult, COLOR_RGBA2GRAY);
}

extern "C" JNIEXPORT void JNICALL Java_com_gpark_facelapse_utils_JNIUtils_bilateralFilter(
        JNIEnv *env,
        jobject instance,
        jlong matAddrInput,
        jlong matAddrResult) {


    Mat &src = *(Mat *) matAddrInput;
    Mat &dst = *(Mat *) matAddrResult;

//    cv::cvtColor(src, dst, cv::COLOR_RGBA2RGB);
    cv::bilateralFilter(src, dst, 9, 75, 75, cv::BORDER_DEFAULT);
}
